﻿using System.Net;
using System.IO;
using System.Text;

namespace SimpleWeb
{
    public class Yoink // If you can come up with a better name tell me or make a pull request or something.
    {
        public string url;
        public string inner_HTML;

        public Yoink( string url )
        {
            this.url = url;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();

			Stream receiveStream = response.GetResponseStream();
			StreamReader readStream = null;

			if (response.CharacterSet == null)
			{
				readStream = new StreamReader(receiveStream);
			}
			else
			{
				readStream = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet));
			}

            this.inner_HTML = readStream.ReadToEnd();

			response.Close();
			readStream.Close();
        }
    }
}
