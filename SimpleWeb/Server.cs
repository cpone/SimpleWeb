﻿using System;
using System.Net;
using System.Threading;
using System.Text;
using System.IO;
using System.Collections.Generic;

namespace SimpleWeb
{
    public class Server
    {
        private string html;
        private string four_o_four = "<h1>404 - Page Not Found</h1>";

        private Dictionary<string, Func<string>> dynamic_pages = new Dictionary<string, Func<string>>();
        private Dictionary<string, string> static_pages = new Dictionary<string, string>();

        private HttpListener listener;
        private Thread server_thread;
        private bool server_running;

        public Server(string address)
        {
            this.listener = new HttpListener();
            this.listener.Prefixes.Add( address );
            this.listener.Start();

            server_thread = new Thread(new ThreadStart(this.ServerLoop));

            Console.WriteLine("Created Server");
        }

        public void AddAddress(string address)
        {
            this.listener.Prefixes.Add( address );
        }

        private void ServerLoop()
        {
            while (this.server_running)
            {
                HttpListenerContext context = this.listener.GetContext();
                HttpListenerResponse response = context.Response;
                HttpListenerRequest request = context.Request;

                Console.WriteLine( "Connection attempt from " + request.UserHostAddress );

                HTMLForPage( request.RawUrl );

                byte[] buffer = Encoding.UTF8.GetBytes(html);

                response.ContentLength64 = buffer.Length;
                Stream st = response.OutputStream;
                st.Write(buffer, 0, buffer.Length);

                context.Response.Close();
            }
        }

        private void HTMLForPage( string page_path ) {
            if (page_path.Substring(page_path.Length - 1) != "/") {
                page_path = page_path + '/';
            }

            try {
                this.html = static_pages[page_path];
            } catch (KeyNotFoundException) {
                try {
                    this.html = dynamic_pages[page_path]();
                } catch (KeyNotFoundException) {
                    this.html = this.four_o_four;
                }
            }
        }

        public void Start()
        {
            this.server_running = true;
            server_thread.Start();
            Console.WriteLine("Started Server");
        }

        public void Kill()
        {
            this.server_running = false;
            server_thread.Abort();
            Console.WriteLine("Killed Server");
        }

        public void Set404( string page_code ) {
            this.four_o_four = page_code;
        }

        public void Set404( Func<string> page_function ) {
            this.four_o_four = page_function();
        }

        public void AddStaticPage( string page_code, string path = "/" ) {
            if ( static_pages.ContainsKey(path) || static_pages.ContainsKey("/" + path) || static_pages.ContainsKey("/" + path + "/") || dynamic_pages.ContainsKey(path) || dynamic_pages.ContainsKey("/" + path ) || dynamic_pages.ContainsKey("/" + path + "/") ) {
                throw new IOException("'" + path + "' has already been used");
            } else {
				if (path[0] != '/')
				{
                    path = '/' + path;
				}
				if (path.Substring(path.Length - 1) != "/")
				{
					path = path + '/';
				}
				
				this.static_pages.Add(path, page_code);
				Console.WriteLine("Added page to: " + path);
            }
        }

        public void AddDynamicPage( Func<string> page_function, string path = "/" )
        {
            if ( static_pages.ContainsKey(path) || static_pages.ContainsKey("/" + path) || dynamic_pages.ContainsKey(path) || dynamic_pages.ContainsKey("/" + path) ) {
                throw new IOException("'" + path + "' has already been used");
            } else {
                if (path[0] == '/')
                {
                    this.dynamic_pages.Add(path, page_function);
                    Console.WriteLine("Added page to: " + path);
                }
                else
                {
                    this.dynamic_pages.Add('/' + path, page_function);
                    Console.WriteLine("Added page to: /" + path);
                }
            }
        }

        public void AddFile( string file_path, string path = "/" ) {
            
        }
    }
}
