# SimpleWeb
A C# .Net module that simplifies and abstracts the process for creating a web server.

I started this project as I wanted a way to create a simple lightweight web server without having to use asp.net and being able to just throw up a website in a couple of lines. The project should support both Mono and .Net. Check out the wiki for some basic documentation.
